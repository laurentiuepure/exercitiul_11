package ro.orangeprojects;

public class TestGeneric {

    public static void main(String[] args) {

        Generic <String> angajat = new Generic<String>("Samuel Jackson");
        Generic <String> post = new Generic<String>("cashier");
        Generic <Integer> salariu = new Generic<Integer>(2000);

        System.out.println("My name is " + angajat.get()); // variabila.functie
        System.out.println("I work as " + post.get() + " at Plaza hotel");
        System.out.println("My salary is valued somewhere around " + salariu.get() + " dollars");

    }

}
